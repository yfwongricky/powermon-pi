#include <math.h>
#include <rfftw.h>
#include "power_calc.h"
#include "conf.h"

struct computation_fifo_queue* computation_fifo_head = NULL;
struct computation_fifo_queue* computation_fifo_tail = NULL;

pthread_mutex_t computation_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t computation_queue_count;

void enqueue_computation_job(unsigned int* vbuffer,unsigned int* ibuffer) {

	struct computation_fifo_queue* new_element = (struct computation_fifo_queue*)malloc(sizeof(struct computation_fifo_queue));
	
	pthread_mutex_lock(&computation_queue_mutex);
	//printf("mutex lock in enqueue_computation_job()\n");
	if(computation_fifo_head == NULL) { // First element
	
		computation_fifo_head = new_element;
		computation_fifo_head->vbuffer_copy = vbuffer;
		computation_fifo_head->ibuffer_copy = ibuffer;
		computation_fifo_head->next = NULL;
		
		computation_fifo_tail = computation_fifo_head;
	
	} else {
	
		//new_element->num = num;
		new_element->vbuffer_copy = vbuffer;
		new_element->ibuffer_copy = ibuffer;
		//memcpy(new_element->vbuffer_copy, vbuffer, sizeof(new_element->vbuffer_copy));
		//memcpy(new_element->ibuffer_copy, ibuffer, sizeof(new_element->ibuffer_copy));
		new_element->next = NULL;
		computation_fifo_tail->next = new_element;
		computation_fifo_tail = new_element;		
	
	}
	pthread_mutex_unlock(&computation_queue_mutex);
	//printf("mutex unlock in enqueue_computation_job()\n");
	
	/* Tell the processing thread there is a job to be processed */
	sem_post(&computation_queue_count);

}

void* calculate_power_metrics_thread(void* thread_arg) {

	/* Rough estimate of index number corresponding to LINE_FREQ in the fft result.
	*  This will be used as the base to refine the estimate further.
	*/
	unsigned int index_line_freq = ceil((double)(LINE_FREQ)*N/SAMPLING_FREQ);
	while(1) {
	
		sem_wait(&computation_queue_count);
		
		pthread_mutex_lock(&computation_queue_mutex);
		//printf("mutex lock in calculate_power()\n");
		
		if(computation_fifo_head != NULL) {
		
			double voltage[N];
			double vrms = 0.0;
			double irms = 0.0;
			double current[N];
			double real_power = 0.0;
			double real_power2 = 0.0;
			double reactive_power = 0.0;
			//double reactive_power2 = 0.0;
			double fft_current[N];
			double fft_voltage[N];
			double power_signal[N];
			double fft_power_signal[N];
			//double spectrum_power_signal[N/2];
			double spectrum_voltage[N/2];
			//double spectrum_current[N/2];
			fftw_complex phase_correction[N/2];
			fftw_complex phase_corrected_fft_current[N/2];
			
			rfftw_plan plan = rfftw_create_plan(N, FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);

			int i = 0;
			for(i = 0; i < N; i++) {
			
				voltage[i] = ((computation_fifo_head->vbuffer_copy[i])-511.0)*(3.60/1023.0)*((240*sqrt(2))/16.6)*((10e3+100e3)/(10e3));
				current[i] = -((computation_fifo_head->ibuffer_copy[i])-511.0)*(3.60/1023.0)*1830.0/100.0;
				power_signal[i] = voltage[i]*current[i];
				vrms += voltage[i]*voltage[i];
				irms += current[i]*current[i];
			
			}
			
			vrms = sqrt(vrms/N);
			irms = sqrt(irms/N);
			
			/* Perform FFT on the voltage signal. The purpose is to find its phase angle with respect to cos(wt)
			*  Used for phase correcting the phase angle of the current signal.
			*/
			rfftw_one(plan, voltage, fft_voltage);
			compute_ss_spectrum(fft_voltage, spectrum_voltage); // Single-sided spectrum (already multiplied by 2)
			
			/* Perform FFT on the current signal.
			*  The result of the computation will be used to calculate real power and reactive power.
			*  Note that:
			*  -> real_power = (1/N)*sum(v(n)*i(n)) = for all w, sum(vpeak/N)*Re(phase_corrected_fft_current @ w)
			*  -> reactive_power = for all w, sum(vpeak/N)*Im(phase_corrected_fft_current @ w)
			*
			*  The reactive power computation considers all harmonics up to the Nyquist frequency.
			*/
			rfftw_one(plan, current, fft_current);
			
			/* Refine the estimate to the fft index corresponding to the line frequency.
			*/
			index_line_freq = max_neighbourhood_index(spectrum_voltage, index_line_freq);
			
			
			real_power2 = 0.0;
			reactive_power = 0.0;
			for(i = 1; i*index_line_freq < N/2; i++) {
			
				// Now with the true index for 50Hz, find the fft value for this index.
				phase_correction[i*index_line_freq].re = fft_voltage[i*index_line_freq]/(spectrum_voltage[i*index_line_freq]*N/2);
				phase_correction[i*index_line_freq].im = -fft_voltage[N-i*index_line_freq]/(spectrum_voltage[i*index_line_freq]*N/2);
				
				phase_corrected_fft_current[i*index_line_freq].re = (fft_current[i*index_line_freq]*phase_correction[i*index_line_freq].re - fft_current[N-i*index_line_freq]*phase_correction[i*index_line_freq].im);
				phase_corrected_fft_current[i*index_line_freq].im = (fft_current[i*index_line_freq]*phase_correction[i*index_line_freq].im + fft_current[N-i*index_line_freq]*phase_correction[i*index_line_freq].re);
				
				real_power2 += phase_corrected_fft_current[i*index_line_freq].re;
				reactive_power += phase_corrected_fft_current[i*index_line_freq].im;
				
			}
			real_power2 *= vrms*sqrt(2)/N;
			reactive_power *= vrms*sqrt(2)/N;
			
			rfftw_one(plan, power_signal, fft_power_signal);
			
			real_power = sqrt(fft_power_signal[0]*fft_power_signal[0])/N;
			//reactive_power = vrms*sqrt(2)*phase_corrected_fft_current[index_line_freq].im/N;
			
			//reactive_power2 = sqrt((vrms*vrms*irms*irms)-real_power*real_power);
			
			rfftw_destroy_plan(plan);
			
			printf("vrms: %lf\n", vrms);
			//printf("P: %lf W P2: %lf W Q: %lf Q2: %lf\n\n", real_power, real_power2, reactive_power, reactive_power2);
			printf("P: %lf W Q: %lf Var\n\n", real_power, reactive_power);
			
			//printf("%d\n", computation_fifo_head->num);
			//printf("Processing...\n");
			struct computation_fifo_queue* new_head = computation_fifo_head->next;
			free(computation_fifo_head->vbuffer_copy);
			free(computation_fifo_head->ibuffer_copy);
			free(computation_fifo_head);
			computation_fifo_head = new_head;
			if(computation_fifo_head == NULL) {
			
				computation_fifo_tail = NULL;
			
			}
		
		}
		
		pthread_mutex_unlock(&computation_queue_mutex);
		//printf("mutex unlock in calculate_power()\n");
		
	}
	return NULL;
	
}

void compute_ss_spectrum(double* fft_buf, double* spectrum_buf) {

	spectrum_buf[0] = 2*sqrt(fft_buf[0]*fft_buf[0])/N;
	if(N % 2 == 0) { // Even
	
		spectrum_buf[N/2] = 2*sqrt(fft_buf[N/2]*fft_buf[N/2])/N;
	
	}
	int i;
	for(i = 1; i < N/2; i++) {
	
		spectrum_buf[i] = 2*sqrt(fft_buf[i]*fft_buf[i] + fft_buf[N-i]*fft_buf[N-i])/N;
	
	}

}

unsigned int max_neighbourhood_index(double* arr, unsigned int index) {

	unsigned int current_max_index = index;
	if(arr[index-1] > arr[current_max_index]) {
	
		current_max_index = index-1;
	
	}
	if(arr[index+1] > arr[current_max_index]) {
	
		current_max_index = index+1;
		
	}
	
	return current_max_index;

}
