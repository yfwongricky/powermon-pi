#ifndef POWER_CALC_H
#define POWER_CALC_H
#include <pthread.h>
#include <semaphore.h>
#include "conf.h"

struct computation_fifo_queue {

	unsigned int* vbuffer_copy;
	unsigned int* ibuffer_copy;
	struct computation_fifo_queue* next;

};

extern struct computation_fifo_queue* computation_fifo_head;
extern struct computation_fifo_queue* computation_fifo_tail;

extern sem_t computation_queue_count;
extern pthread_mutex_t computation_queue_mutex;

void enqueue_computation_job(unsigned int* vbuffer,unsigned int* ibuffer);

void* calculate_power_metrics_thread(void* thread_arg);

void compute_ss_spectrum(double* fft_buf, double* spectrum_buf);

unsigned int max_neighbourhood_index(double* arr, unsigned int index);

#endif
