#ifndef CONF_H
#define CONF_H

/* Configuration Info */
#define SAMPLE_BUFFER_SIZE 1024
#define RAW_LOGGING_ENABLED 0
#define SAMPLING_FREQ 4200
#define LINE_FREQ 50
#define N SAMPLE_BUFFER_SIZE

#endif
