#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <linux/serial.h>
#include <stropts.h>
#include <termios.h>
#include <asm-generic/ioctls.h>
#include <pthread.h>
#include <semaphore.h>
#include <math.h>
#include <rfftw.h>
#include "power_calc.h"
#include "conf.h"

void set_custom_baud_rate(int port_fd, int custom_baud_rate);

void get_samples_from_uart(int port_fd);

int main(int argc, char* argv[]) {

	pthread_t thread_id;

	const char* serial_port = "/dev/ttyUSB0";
	
	unsigned int port_fd = open(serial_port, O_RDONLY);
	if(port_fd == -1) {
	
		perror("Unable to open serial connection");
		exit(1);
	
	}
	
	set_custom_baud_rate(port_fd, 1000000);
	
	/* Create background thread for computation of power metrics */
	pthread_create(&thread_id, NULL, &calculate_power_metrics_thread, NULL);
	
	get_samples_from_uart(port_fd);
	
	close(port_fd);
	return 0;

}

void set_custom_baud_rate(int port_fd, int custom_baud_rate) {

	/*
		Reference:
			http://stackoverflow.com/questions/4968529/how-to-set-baud-rate-to-307200-on-linux
			http://ftdi-usb-sio.sourceforge.net/
	*/
	struct termios options;
	struct serial_struct ss;
	
	// int closest_speed;
	
	ioctl(port_fd, TIOCGSERIAL, &ss);
	ss.flags = (ss.flags & ~ASYNC_SPD_MASK) | ASYNC_SPD_CUST;
	/*ss.custom_divisor = (ss.baud_base + (custom_baud_rate / 2)) / custom_baud_rate;
	closest_speed = ss.baud_base / ss.custom_divisor;

	if (closest_speed < custom_baud_rate * 98 / 100 || closest_speed > custom_baud_rate * 102 / 100) {
		fprintf(stderr, "Cannot set serial port speed to %d. Closest possible is %d\n", custom_baud_rate, closest_speed);
	}*/
	ss.custom_divisor = ss.baud_base/custom_baud_rate;
	
	ioctl(port_fd, TIOCSSERIAL, &ss);
	
	cfsetispeed(&options, B38400);
	cfsetospeed(&options, B38400);
	options.c_cflag = CLOCAL;
	
	tcsetattr(port_fd, TCSANOW, &options);

}

void get_samples_from_uart(int port_fd) {

	int nbytes = 0;
	char buf[1000] = "";
	unsigned int vtemp_buffer = 0;
	unsigned int itemp_buffer = 0;
	unsigned int vbuffer_index = 0;
	unsigned int ibuffer_index = 0;
	unsigned int vbuffer[SAMPLE_BUFFER_SIZE+200];
	unsigned int ibuffer[SAMPLE_BUFFER_SIZE+200];
	FILE* vlog_file;
	FILE* ilog_file;
	if(RAW_LOGGING_ENABLED) {
		vlog_file = fopen("v_samples.log","w");
		ilog_file = fopen("i_samples.log","w");
		if(vlog_file == NULL || ilog_file == NULL) {
		
			perror("Unable to create raw log files");
			exit(1);
		
		}
	}
	
	/* Initialize semaphore to a value of 0.
	*  Used for notifying the processing thread that a block of voltage samples
	*  and current samples have been extracted from the uart frames. As such,
	*  computation of power metrics should proceed.
	*/
	sem_init(&computation_queue_count, 0, 0);

	/* Actual extraction operation */
	while((nbytes = read(port_fd, buf, 1)) > 0) {

		if((buf[0] & 0x60) == 0x00) { // Voltage		
		
			if((buf[0] & 0x80) == 0x00) { // Voltage first frame
			
				vtemp_buffer = (buf[0] & 0x1F);
			
			} else if((buf[0] & 0x80) == 0x80) { // Voltage second frame
			
				vbuffer[vbuffer_index] = ((buf[0] & 0x1F) << 5) | vtemp_buffer;
				if(RAW_LOGGING_ENABLED) fprintf(vlog_file, "%d ", vbuffer[vbuffer_index]);
				vbuffer_index++;
			
			}
		
		} else if((buf[0] & 0x60) == 0x20) { // Current
		
			if((buf[0] & 0x80) == 0x00) { // Current first frame
			
				itemp_buffer = (buf[0] & 0x1F);
			
			} else if((buf[0] & 0x80) == 0x80) { // Current second frame
			
				ibuffer[ibuffer_index] = ((buf[0] & 0x1F) << 5) | itemp_buffer;
				if(RAW_LOGGING_ENABLED) fprintf(ilog_file, "%d ", ibuffer[ibuffer_index]);
				ibuffer_index++;
			
			}
		
		}
		
		if(vbuffer_index >= SAMPLE_BUFFER_SIZE && ibuffer_index >= SAMPLE_BUFFER_SIZE) {
		
			vbuffer_index = 0;
			ibuffer_index = 0;
			
			/* Create copies of sample buffers. Will be used by processing thread
			*  while the original is reused for the next round of extraction.
			*/
			unsigned int* vbuffer_copy = (unsigned int*)malloc((SAMPLE_BUFFER_SIZE+200)*sizeof(unsigned int));
			memcpy(vbuffer_copy, vbuffer, sizeof(vbuffer));
			unsigned int* ibuffer_copy = (unsigned int*)malloc((SAMPLE_BUFFER_SIZE+200)*sizeof(unsigned int));
			memcpy(ibuffer_copy, ibuffer, sizeof(ibuffer));
			/* Add computation job to a queue */
			enqueue_computation_job(vbuffer_copy, ibuffer_copy);
		
		}
	
	}
	
	if(RAW_LOGGING_ENABLED) {
		fclose(vlog_file);
		fclose(ilog_file);
	}

}