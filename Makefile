CC=gcc
CFLAGS=-Wall -Os
LDFLAGS=-lpthread -lrfftw -lfftw
SOURCE_DIR=src
SOURCES=$(SOURCE_DIR)/*.c
EXECUTABLE=powermon-pi

all:
	$(CC) $(SOURCES) $(CFLAGS) $(LDFLAGS) -o $(EXECUTABLE)

clean:
	-@rm -rf $(EXECUTABLE) 2>/dev/null || true
